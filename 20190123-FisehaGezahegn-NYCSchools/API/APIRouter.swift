//
//  APIRouter.swift
//  20190123-FisehaGezahegn-NYCSchools
//
//  Created by Fiseha Gezahegn on 1/23/19.
//  Copyright © 2019 Chase. All rights reserved.
//


import Foundation
import Alamofire

enum APIRouter {
    static let baseURLString = URLs.baseURL
    case getSchools()
    case getSATScores()
    
    var method: HTTPMethod {
        return .get
    }
    
    var path: String {
        switch self {
        case .getSchools():
            return URLs.getSchools
        case .getSATScores:
            return URLs.getSATScores
        }
    }
    
}

extension APIRouter: URLRequestConvertible {
    func request() -> DataRequest {
        return Alamofire.request(self)
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try APIRouter.baseURLString.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.timeoutInterval = 60
        urlRequest.httpMethod = method.rawValue
        return urlRequest
    }
}
