//
//  SchoolCell.swift
//  20190123-FisehaGezahegn-NYCSchools
//
//  Created by Fiseha Gezahegn on 1/23/19.
//  Copyright © 2019 Chase. All rights reserved.
//


import FoldingCell
import UIKit

class SchoolCell: FoldingCell {
    
    @IBOutlet var closeNumberLabel: UILabel!
    @IBOutlet var openNumberLabel: UILabel!
    @IBOutlet var schoolNameLabel: UILabel!
    @IBOutlet var schoolDetailNameLabel: UILabel!
    @IBOutlet var schoolEndTimeLabel: UILabel!
    @IBOutlet var schoolNeighborhoodLabel: UILabel!
    @IBOutlet var schoolFinalGradesLabel: UILabel!
    @IBOutlet var schoolCodeLabel: UILabel!
    @IBOutlet var schoolTotalStudentsLabel: UILabel!
    @IBOutlet var schoolCityLabel: UILabel!
    @IBOutlet var schoolRequirementLabel: UILabel!
    @IBOutlet var numOfSatTestTakersLabel: UILabel!
    @IBOutlet var satCriticalReadingAvgScoreLabel: UILabel!
    @IBOutlet var satMathAvgScoreLabel: UILabel!
    @IBOutlet var satWritingAvgScoreLabel: UILabel!
    @IBOutlet weak var mapImageView:UIImageView!
    @IBOutlet var contactButton: UIButton!

    var phoneNumber : String = String("")
    
    override func awakeFromNib() {
        foregroundView.layer.cornerRadius = 10
        foregroundView.layer.masksToBounds = true
        super.awakeFromNib()
    }
    
    override func animationDuration(_ itemIndex: NSInteger, type _: FoldingCell.AnimationType) -> TimeInterval {
        let durations = [0.26, 0.2, 0.2]
        return durations[itemIndex]
    }
    
    
}


extension SchoolCell {
    
    @IBAction func makeCall(_: AnyObject) {
        
    }
    
    @IBAction func buttonHandler(_: AnyObject) {
        print("tap")
        self.phoneNumber.makeACall()
    }
    
    func updateWithImage(img:UIImage?) {
        if let correctImage = img {
            mapImageView.image = correctImage
        } else {
            mapImageView.image = nil
        }
    }
}
