//
//  Info.swift
//  20190123-FisehaGezahegn-NYCs
//
//  Created by Thomas Annan on 1/24/19.
//  Copyright © 2019 Chase. All rights reserved.
//

import Foundation


struct Info {
    var name: String
    var endTime:String
    var neighborhood:String
    var finalGrades:String
    var code:String
    var totalStudents:String
    var city:String
    var numOfSatTestTakers:String
    var satCriticalReadingAvgScore:String
    var satMathAvgScore:String
    var satWritingAvgScore:String
    var location:Location
    
    
    init(name: String,
     endTime:String,
     neighborhood:String,
     finalGrades:String,
     code:String,
     totalStudents:String,
     city:String,
     numOfSatTestTakers:String,
     satCriticalReadingAvgScore:String,
     satMathAvgScore:String,
     satWritingAvgScore:String,
     location:Location) {
        self.name = name
        self.endTime = endTime
        self.neighborhood = neighborhood
        self.finalGrades = finalGrades
        self.code = code
        self.totalStudents = totalStudents
        self.city = city
        self.numOfSatTestTakers = numOfSatTestTakers
        self.satCriticalReadingAvgScore = satCriticalReadingAvgScore
        self.satMathAvgScore = satMathAvgScore
        self.satWritingAvgScore = satWritingAvgScore
    }
}
