//
//  School.swift
//  20190123-FisehaGezahegn-NYCSchools
//
//  Created by Fiseha Gezahegn on 1/23/19.
//  Copyright © 2019 Chase. All rights reserved.
//

import Foundation


struct School : Codable {
    let academicopportunities1 : String?
    let academicopportunities2 : String?
    let admissionspriority11 : String?
    let admissionspriority21 : String?
    let admissionspriority31 : String?
    let attendance_rate : String?
    let bbl : String?
    let bin : String?
    let boro : String?
    let borough : String?
    let building_code : String?
    let bus : String?
    let census_tract : String?
    let city : String?
    let code1 : String?
    let community_board : String?
    let council_district : String?
    let dbn : String?
    let directions1 : String?
    let ell_programs : String?
    let extracurricular_activities : String?
    let fax_number : String?
    let finalgrades : String?
    let grade9geapplicants1 : String?
    let grade9geapplicantsperseat1 : String?
    let grade9gefilledflag1 : String?
    let grade9swdapplicants1 : String?
    let grade9swdapplicantsperseat1 : String?
    let grade9swdfilledflag1 : String?
    let grades2018 : String?
    let interest1 : String?
    let latitude : String?
    let location : String?
    let longitude : String?
    let method1 : String?
    let neighborhood : String?
    let nta : String?
    let offer_rate1 : String?
    let overview_paragraph : String?
    let pct_stu_enough_variety : String?
    let pct_stu_safe : String?
    let phone_number : String?
    let primary_address_line_1 : String?
    let program1 : String?
    let requirement1_1 : String?
    let requirement2_1 : String?
    let requirement3_1 : String?
    let requirement4_1 : String?
    let requirement5_1 : String?
    let school_10th_seats : String?
    let school_accessibility_description : String?
    let school_email : String?
    let school_name : String?
    let school_sports : String?
    let seats101 : String?
    let seats9ge1 : String?
    let seats9swd1 : String?
    let state_code : String?
    let subway : String?
    let total_students : String?
    let website : String?
    let zip : String?
    
    enum CodingKeys: String, CodingKey {
        
        case academicopportunities1 = "academicopportunities1"
        case academicopportunities2 = "academicopportunities2"
        case admissionspriority11 = "admissionspriority11"
        case admissionspriority21 = "admissionspriority21"
        case admissionspriority31 = "admissionspriority31"
        case attendance_rate = "attendance_rate"
        case bbl = "bbl"
        case bin = "bin"
        case boro = "boro"
        case borough = "borough"
        case building_code = "building_code"
        case bus = "bus"
        case census_tract = "census_tract"
        case city = "city"
        case code1 = "code1"
        case community_board = "community_board"
        case council_district = "council_district"
        case dbn = "dbn"
        case directions1 = "directions1"
        case ell_programs = "ell_programs"
        case extracurricular_activities = "extracurricular_activities"
        case fax_number = "fax_number"
        case finalgrades = "finalgrades"
        case grade9geapplicants1 = "grade9geapplicants1"
        case grade9geapplicantsperseat1 = "grade9geapplicantsperseat1"
        case grade9gefilledflag1 = "grade9gefilledflag1"
        case grade9swdapplicants1 = "grade9swdapplicants1"
        case grade9swdapplicantsperseat1 = "grade9swdapplicantsperseat1"
        case grade9swdfilledflag1 = "grade9swdfilledflag1"
        case grades2018 = "grades2018"
        case interest1 = "interest1"
        case latitude = "latitude"
        case location = "location"
        case longitude = "longitude"
        case method1 = "method1"
        case neighborhood = "neighborhood"
        case nta = "nta"
        case offer_rate1 = "offer_rate1"
        case overview_paragraph = "overview_paragraph"
        case pct_stu_enough_variety = "pct_stu_enough_variety"
        case pct_stu_safe = "pct_stu_safe"
        case phone_number = "phone_number"
        case primary_address_line_1 = "primary_address_line_1"
        case program1 = "program1"
        case requirement1_1 = "requirement1_1"
        case requirement2_1 = "requirement2_1"
        case requirement3_1 = "requirement3_1"
        case requirement4_1 = "requirement4_1"
        case requirement5_1 = "requirement5_1"
        case school_10th_seats = "school_10th_seats"
        case school_accessibility_description = "school_accessibility_description"
        case school_email = "school_email"
        case school_name = "school_name"
        case school_sports = "school_sports"
        case seats101 = "seats101"
        case seats9ge1 = "seats9ge1"
        case seats9swd1 = "seats9swd1"
        case state_code = "state_code"
        case subway = "subway"
        case total_students = "total_students"
        case website = "website"
        case zip = "zip"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        academicopportunities1 = try values.decodeIfPresent(String.self, forKey: .academicopportunities1)
        academicopportunities2 = try values.decodeIfPresent(String.self, forKey: .academicopportunities2)
        admissionspriority11 = try values.decodeIfPresent(String.self, forKey: .admissionspriority11)
        admissionspriority21 = try values.decodeIfPresent(String.self, forKey: .admissionspriority21)
        admissionspriority31 = try values.decodeIfPresent(String.self, forKey: .admissionspriority31)
        attendance_rate = try values.decodeIfPresent(String.self, forKey: .attendance_rate)
        bbl = try values.decodeIfPresent(String.self, forKey: .bbl)
        bin = try values.decodeIfPresent(String.self, forKey: .bin)
        boro = try values.decodeIfPresent(String.self, forKey: .boro)
        borough = try values.decodeIfPresent(String.self, forKey: .borough)
        building_code = try values.decodeIfPresent(String.self, forKey: .building_code)
        bus = try values.decodeIfPresent(String.self, forKey: .bus)
        census_tract = try values.decodeIfPresent(String.self, forKey: .census_tract)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        code1 = try values.decodeIfPresent(String.self, forKey: .code1)
        community_board = try values.decodeIfPresent(String.self, forKey: .community_board)
        council_district = try values.decodeIfPresent(String.self, forKey: .council_district)
        dbn = try values.decodeIfPresent(String.self, forKey: .dbn)
        directions1 = try values.decodeIfPresent(String.self, forKey: .directions1)
        ell_programs = try values.decodeIfPresent(String.self, forKey: .ell_programs)
        extracurricular_activities = try values.decodeIfPresent(String.self, forKey: .extracurricular_activities)
        fax_number = try values.decodeIfPresent(String.self, forKey: .fax_number)
        finalgrades = try values.decodeIfPresent(String.self, forKey: .finalgrades)
        grade9geapplicants1 = try values.decodeIfPresent(String.self, forKey: .grade9geapplicants1)
        grade9geapplicantsperseat1 = try values.decodeIfPresent(String.self, forKey: .grade9geapplicantsperseat1)
        grade9gefilledflag1 = try values.decodeIfPresent(String.self, forKey: .grade9gefilledflag1)
        grade9swdapplicants1 = try values.decodeIfPresent(String.self, forKey: .grade9swdapplicants1)
        grade9swdapplicantsperseat1 = try values.decodeIfPresent(String.self, forKey: .grade9swdapplicantsperseat1)
        grade9swdfilledflag1 = try values.decodeIfPresent(String.self, forKey: .grade9swdfilledflag1)
        grades2018 = try values.decodeIfPresent(String.self, forKey: .grades2018)
        interest1 = try values.decodeIfPresent(String.self, forKey: .interest1)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        location = try values.decodeIfPresent(String.self, forKey: .location)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        method1 = try values.decodeIfPresent(String.self, forKey: .method1)
        neighborhood = try values.decodeIfPresent(String.self, forKey: .neighborhood)
        nta = try values.decodeIfPresent(String.self, forKey: .nta)
        offer_rate1 = try values.decodeIfPresent(String.self, forKey: .offer_rate1)
        overview_paragraph = try values.decodeIfPresent(String.self, forKey: .overview_paragraph)
        pct_stu_enough_variety = try values.decodeIfPresent(String.self, forKey: .pct_stu_enough_variety)
        pct_stu_safe = try values.decodeIfPresent(String.self, forKey: .pct_stu_safe)
        phone_number = try values.decodeIfPresent(String.self, forKey: .phone_number)
        primary_address_line_1 = try values.decodeIfPresent(String.self, forKey: .primary_address_line_1)
        program1 = try values.decodeIfPresent(String.self, forKey: .program1)
        requirement1_1 = try values.decodeIfPresent(String.self, forKey: .requirement1_1)
        requirement2_1 = try values.decodeIfPresent(String.self, forKey: .requirement2_1)
        requirement3_1 = try values.decodeIfPresent(String.self, forKey: .requirement3_1)
        requirement4_1 = try values.decodeIfPresent(String.self, forKey: .requirement4_1)
        requirement5_1 = try values.decodeIfPresent(String.self, forKey: .requirement5_1)
        school_10th_seats = try values.decodeIfPresent(String.self, forKey: .school_10th_seats)
        school_accessibility_description = try values.decodeIfPresent(String.self, forKey: .school_accessibility_description)
        school_email = try values.decodeIfPresent(String.self, forKey: .school_email)
        school_name = try values.decodeIfPresent(String.self, forKey: .school_name)
        school_sports = try values.decodeIfPresent(String.self, forKey: .school_sports)
        seats101 = try values.decodeIfPresent(String.self, forKey: .seats101)
        seats9ge1 = try values.decodeIfPresent(String.self, forKey: .seats9ge1)
        seats9swd1 = try values.decodeIfPresent(String.self, forKey: .seats9swd1)
        state_code = try values.decodeIfPresent(String.self, forKey: .state_code)
        subway = try values.decodeIfPresent(String.self, forKey: .subway)
        total_students = try values.decodeIfPresent(String.self, forKey: .total_students)
        website = try values.decodeIfPresent(String.self, forKey: .website)
        zip = try values.decodeIfPresent(String.self, forKey: .zip)
    }
    
}
