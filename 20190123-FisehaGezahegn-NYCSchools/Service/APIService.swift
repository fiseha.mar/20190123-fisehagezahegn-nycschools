//
//  SATScoreService.swift
//  20190123-FisehaGezahegn-NYCSchools
//
//  Created by Fiseha Gezahegn on 1/23/19.
//  Copyright © 2019 Chase. All rights reserved.
//

import Foundation
import Alamofire


struct APIService{
    
    public static func getAllSATScores(completion: @escaping ([SATScore]?) -> Void) {
        APIRouter.getSATScores()
            .request()
            .responseCodable{(result: Result<[SATScore]>) in
                switch result {
                case .failure( _):
                    completion(nil)
                case .success(let allScores):
                    completion(allScores)
                }
        }
    }
    
    public static func getAllSchools(completion: @escaping ([School]?) -> Void) {
        APIRouter.getSchools()
            .request()
            .responseCodable{(result: Result<[School]>) in
                switch result {
                case .failure( _):
                    completion(nil)
                case .success(let allSchools):
                    completion(allSchools)
                }
        }
    }
}
