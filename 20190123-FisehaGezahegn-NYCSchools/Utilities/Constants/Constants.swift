//
//  Constants.swift
//  20190123-FisehaGezahegn-NYCSchools
//
//  Created by Fiseha Gezahegn on 1/23/19.
//  Copyright © 2019 Chase. All rights reserved.
//

import Foundation



struct URLs {
    static let baseURL = "https://data.cityofnewyork.us/resource/"
    static let getSchools = "97mf-9njv.json"
    static let getSATScores = "734v-jeq5.json"  
}
