//
//  Alamofire+Extension.swift
//  20190123-FisehaGezahegn-NYCSchools
//
//  Created by Fiseha Gezahegn on 1/23/19.
//  Copyright © 2019 Chase. All rights reserved.
//

import Foundation
import Alamofire


extension DataRequest {
    
    @discardableResult
    public func responseCodable<T: Codable>(_ type: T.Type = T.self, queue: DispatchQueue? = nil, jsonDecoder: JSONDecoder = JSONDecoder(), completionHandler: @escaping (Result<T>) -> Void) -> DataRequest {
        return self.responseData(queue: queue) {
            switch $0.result {
            case .failure(let error):
                completionHandler(.failure(error))
            case .success(let data):
                do {
                    let decodedData = try jsonDecoder.decode(T.self, from: data)
                    completionHandler(.success(decodedData))
                } catch(let error) {
                    completionHandler(.failure(error))
                }
            }
            
        }
    }
}
