//
//  MainTableViewController.swift
//  20190123-FisehaGezahegn-NYCSchools
//
//  Created by Fiseha Gezahegn on 1/23/19.
//  Copyright © 2019 Chase. All rights reserved.
//


import FoldingCell
import UIKit
import MapKit



struct Location {
    let latitude:Double
    let longitude:Double
}

class MainTableViewController: UITableViewController {
    
    enum Const {
        static let closeCellHeight: CGFloat = 179
        static let openCellHeight: CGFloat = 488
        static let rowsCount = 100
    }
    
    var cellHeights: [CGFloat] = []
    
    var allSchools: [School] = []
    var allSATScores: [SATScore] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
           self.tableView.reloadData()
       }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.getData()
            self.tableView.reloadData()
        }
    }
    
    private func getData(){
        
        APIService.getAllSchools{(response) in
            //print(response!)
            if let response = response{
                self.allSchools = response
                APIService.getAllSATScores{(SATResponse) in
                    // print(response!)
                    if let response = SATResponse{
                        self.allSATScores = response
                        
                        self.setup()
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    }
                }
            }
        }
        
    }
    
    
    func mapSnapper(loc:Location) -> MKMapSnapshotter? {
        var mapShotter:MKMapSnapshotter? = nil
        let mapSnapOptions:MKMapSnapshotter.Options = MKMapSnapshotter.Options()
        let location = CLLocationCoordinate2D(latitude: CLLocationDegrees.init(loc.latitude), longitude: CLLocationDegrees.init(loc.longitude))
        let region = MKCoordinateRegion(center: location, latitudinalMeters: 300, longitudinalMeters: 300)
        mapSnapOptions.region = region
        mapSnapOptions.scale = UIScreen.main.scale
        mapSnapOptions.size = CGSize(width: 200.0, height: 200.0)
        mapSnapOptions.showsPointsOfInterest = true
        mapShotter = MKMapSnapshotter.init(options: mapSnapOptions)
        
        return mapShotter
    }
    
    
    
    private func setup() {
        cellHeights = Array(repeating: Const.closeCellHeight, count: allSchools.count)
        tableView.estimatedRowHeight = Const.closeCellHeight
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "background"))
        if #available(iOS 10.0, *) {
            tableView.refreshControl = UIRefreshControl()
            tableView.refreshControl?.addTarget(self, action: #selector(refreshHandler), for: .valueChanged)
        }
    }
    
    @objc func refreshHandler() {
        let deadlineTime = DispatchTime.now() + .seconds(1)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: { [weak self] in
            if #available(iOS 10.0, *) {
                self?.tableView.refreshControl?.endRefreshing()
            }
            self?.tableView.reloadData()
        })
    }
}

// MARK: - TableView

extension MainTableViewController {
    
    override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return allSchools.count
    }
    
    override func tableView(_: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard case let cell as SchoolCell = cell else {
            return
        }
        
        cell.backgroundColor = .clear
        
        if cellHeights[indexPath.row] == Const.closeCellHeight {
            cell.unfold(false, animated: false, completion: nil)
        } else {
            cell.unfold(true, animated: false, completion: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FoldingCell", for: indexPath) as! SchoolCell
        let durations: [TimeInterval] = [0.26, 0.2, 0.2]
        cell.durationsForExpandedState = durations
        cell.durationsForCollapsedState = durations
        
        let school = allSchools[indexPath.row]
        cell.schoolNameLabel.text = school.school_name
        cell.schoolNeighborhoodLabel.text = school.neighborhood
        //cell.schoolEndTimeLabel.text = school.
        cell.schoolCityLabel.text = school.city
        cell.schoolCodeLabel.text = school.code1
        cell.schoolTotalStudentsLabel.text = school.total_students
        cell.schoolFinalGradesLabel.text = school.finalgrades
        cell.schoolDetailNameLabel.text = school.school_name
        cell.openNumberLabel.text = school.school_name
        cell.schoolRequirementLabel.text = school.requirement1_1
        
        if let phoneNumber = school.phone_number{
            cell.phoneNumber = phoneNumber
        }
        
        
        if let lat = school.latitude, let longt = school.longitude{
            let locCoords = Location.init(latitude: Double(lat) ?? 0.00, longitude: Double(longt) ?? 0.00)
            let snapShotter = self.mapSnapper(loc: locCoords)
            snapShotter?.start(completionHandler: { (snapshot, error) in
                guard let img = snapshot?.image else { return }
                cell.updateWithImage(img: img)
               
            })
        }
      
        
        
        let satScore = allSATScores.first(where:  { $0.dbn == school.dbn })
        cell.satMathAvgScoreLabel.text = satScore?.sat_math_avg_score
        cell.satWritingAvgScoreLabel.text = satScore?.sat_writing_avg_score
        cell.satCriticalReadingAvgScoreLabel.text = satScore?.sat_critical_reading_avg_score
        cell.numOfSatTestTakersLabel.text = satScore?.num_of_sat_test_takers
        
        return cell
    }
    
    override func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath.row]
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! FoldingCell
        
        if cell.isAnimating() {
            return
        }
        
        var duration = 0.0
        let cellIsCollapsed = cellHeights[indexPath.row] == Const.closeCellHeight
        if cellIsCollapsed {
            cellHeights[indexPath.row] = Const.openCellHeight
            cell.unfold(true, animated: true, completion: nil)
            duration = 0.5
        } else {
            cellHeights[indexPath.row] = Const.closeCellHeight
            cell.unfold(false, animated: true, completion: nil)
            duration = 0.8
        }
        
        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: { () -> Void in
            tableView.beginUpdates()
            tableView.endUpdates()
        }, completion: nil)
    }
}
